FROM nginxinc/nginx-unprivileged:1.22.1
COPY zoo /usr/share/nginx/html/
COPY zoo2 /usr/share/nginx/html/zoo/
COPY default.conf /etc/nginx/conf.d
RUN cat /etc/nginx/conf.d/default.conf
RUN cat /etc/nginx/nginx.conf
ENV SOME=SOME2
ENV DT_RELEASE_VERSION=0.88
USER nginx
EXPOSE 8080